﻿using System;
using System.Diagnostics;
using System.IO;

public class index
{

	string current_dir;
	string dstool = "\\tools\\3dstool.exe";
	string omkrom = "\\tools\\omakerom.exe";
	string mkrom = "\\tools\\makerom.exe";
	string ctrtool = "\\tools\\ctrtool.exe";
	string rsfgen = "\\tools\\rsfgen2.exe";
	string log_file;
	StreamWriter filelog;


	string full_file;
	string dir_file;
	string name_file;
	public index(string[] args)
	{


		full_file = args[0];
		current_dir = System.Environment.GetEnvironmentVariable("systemdrive") + "\\ciabuilder\\";

		Directory.CreateDirectory(System.Environment.GetEnvironmentVariable("systemdrive") + "\\ciabuilder\\log");
		log_file = current_dir + "\\log\\log.txt";
		File.Delete(log_file);






		if (args.Length > 0)
		{
			FileInfo fifo = new FileInfo(full_file);
			dir_file = fifo.DirectoryName;
			name_file = fifo.Name.Replace(".cci", "");


			prepare();
			prepbuild();
			build3ds();
			buildcia();
			cleanup();


		}


		debug("end line");
		System.Windows.Forms.MessageBox.Show("Ready","Ready",System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Asterisk);

	}

	public void prepare()
	{
		log("----------------- Prepare and extract files ---------------------");
		startProce(ctrtool, "--exefsdir=exefs --romfsdir=romfs --exheader=exheader.bin " + full_file);

		if (!File.Exists(dir_file + "\\" + name_file + ".rsf"))
		{
			File.Copy(current_dir + "\\tools\\auto2.rsf","game.rsf", true);

			startProce(rsfgen, full_file);
			File.Move("game.rsf", dir_file + "\\" + name_file + ".rsf");
			debug("Edit File " + name_file + ".rsf" + "\nand press any key to continue");
			Console.ReadKey();
		}
	}

	public void prepbuild()
	{
		log("--------------------- Prepare for build --------------------\n");

		startProce(dstool, "-cvtf romfs romfs.bin --romfs-dir romfs");


	}

	public void build3ds()
	{
		log("--------------------- Build 3DS File --------------------");

		log(mkrom + " -f cxi -o game.cxi -rsf " + dir_file + "\\" + name_file + ".rsf" + " -code " + dir_file + "\\" + "exefs\\code.bin -icon " + dir_file + "\\" + "exefs\\icon.bin " +
			"-banner " + dir_file + "\\" + "exefs\\banner.bin -exheader " + dir_file + "\\" + "exheader.bin -romfs " + dir_file + "\\" + "romfs.bin");
		
		startProce(mkrom, "-f cxi -o game.cxi -rsf " + dir_file + "\\" + name_file + ".rsf" + " -code "+dir_file + "\\" + "exefs\\code.bin -icon "+dir_file + "\\" + "exefs\\icon.bin " +
				   "-banner "+dir_file + "\\" +"exefs\\banner.bin -exheader "+dir_file + "\\" +"exheader.bin -romfs "+dir_file + "\\" +"romfs.bin");
		File.Copy("game.cxi", dir_file + "\\" + name_file + ".3ds",true);


	}
	public void buildcia()
	{
        log("--------------------- Build CIA Installer --------------------");

		log(omkrom + " -f cia -o game.cia -content game.cxi:0");
        startProce(omkrom, "-f cia -o game.cia -content game.cxi:0");
		if (File.Exists(dir_file + "\\" + name_file + ".cia"))
		{
			File.Delete(dir_file + "\\" + name_file + ".cia");
		}
		File.Move("game.cia", dir_file + "\\" + name_file + ".cia");
	}


	public void cleanup() {

		

	}

	public void startProce(string filename, string args)
	{


		Process prc = new Process();
		prc.StartInfo.FileName = current_dir + filename;
		prc.StartInfo.UseShellExecute = false;
		prc.StartInfo.CreateNoWindow = false;
		prc.StartInfo.RedirectStandardOutput = true;
		prc.StartInfo.Arguments = args;
		debug(filename + " " + args);
		prc.Start();

		string outline = prc.StandardOutput.ReadToEnd();
		debug(outline);

		File.AppendAllText(log_file, "----------------------------------------------------------------------------------------------------");
		File.AppendAllText(log_file, "\n");

		File.AppendAllText(log_file, "program: " + filename);
		File.AppendAllText(log_file, "\n");

		File.AppendAllText(log_file, outline);

		File.AppendAllText(log_file, "\n");

	}


	public void log(string tx)
	{
		File.AppendAllText(log_file, tx);
		File.AppendAllText(log_file, "\n");

	}
	public static void debug(string tx)
	{

		Console.WriteLine(tx);
	}
}

