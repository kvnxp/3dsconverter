﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.IO;
namespace CTRLoad
{
	public class index
	{
		public index(string[] args)
		{

			string is_ctr_dir = System.Environment.GetEnvironmentVariable("IS_CTR_DIR") + "X86\\bin";
			string ctremu_file = is_ctr_dir + "\\ctremu.txt";

			if (!File.Exists(ctremu_file))
			{
				OpenFileDialog filedialog = new OpenFileDialog();
				filedialog.Filter = "Applicacion|*.exe";
				DialogResult result = filedialog.ShowDialog();
				File.WriteAllText(ctremu_file, filedialog.FileName);
			}

			string ctremu_dir = File.ReadAllLines(ctremu_file)[0];


			string file = System.Environment.CurrentDirectory+"\\"+args[1];

			if (File.Exists(ctremu_dir))
			{
				MessageBox.Show("load emu " + ctremu_dir  );
				MessageBox.Show("load game " + file);
				Process prc = new Process();
				prc.StartInfo.FileName = ctremu_dir;
				prc.StartInfo.Arguments = file;
				prc.StartInfo.UseShellExecute = true;
				prc.Start();
				prc.WaitForExit();

				//System.Diagnostics.Process.Start(ctremu_dir, projectdir + file);

			}
			else
			{
				MessageBox.Show("Cant not found a emulator", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
			}
		}
	}
}
