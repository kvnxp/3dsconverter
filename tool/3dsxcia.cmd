@echo off
set game=%1

tools\cxitool  %game% game.cxi
tools\omakerom -f cia -o game.cia -rsf %~d1%~p1%~n1.rsf -target t -i game.cxi:0:0
move game.cxi %~d1%~p1%~n1.3ds
move game.cia %~d1%~p1%~n1.cia
pause 