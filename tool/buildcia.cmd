@echo off

set rom=%1

echo %~d1%~p1
if %1 EQU "" goto exit


:main
tools\ctrtool --exefsdir=exefs --romfsdir=romfs --exheader=exheader.bin %rom%

if not exist %~d1%~p1%~n1.rsf ( 
copy tools\auto2.rsf game.rsf 
 tools\rsfgen2.exe %rom% 
 move game.rsf %~d1%~p1%~n1.rsf
 echo Edit %~n1.rsf  file 
 pause 
)

#cls 
echo "Rebuilding to cia"

tools\3dstool -cvtf romfs romfs.bin --romfs-dir romfs/
echo.
echo build rom cxi 
tools\makerom -f cxi -o game.cxi -rsf %~d1%~p1%~n1.rsf -code exefs/code.bin -icon exefs/icon.bin -banner exefs/banner.bin -exheader exheader.bin -romfs romfs.bin
echo.
echo build cia 
tools\omakerom -f cia -o game.cia -content game.cxi:0

move game.cxi %~d1%~p1%~n1.3ds
move game.cia %~d1%~p1%~n1.cia

:cleanup
#cls
echo Cleaning Up Files..
if exist "banner.bin" del /F /Q "banner.bin"
if exist "code.bin" del /F /Q "code.bin"
if exist "exefs.bin" del /F /Q "exefs.bin"
if exist "exheader.bin" del /F /Q "exheader.bin"
if exist "icon.bin" del /F /Q "icon.bin"
if exist "logo.bin" del /F /Q "logo.bin"
if exist "romfs.bin" del /F /Q "romfs.bin"
#cls
echo Files Cleaned Up..
echo.


:exit





